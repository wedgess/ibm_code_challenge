# IBM Code Challenge #

## Task 1 ##

* In a Web Page create a reusable JavaScript control which will display a list of 6 players along with 5 skills and a rating of each skill in a pop up business card. The list is read from a JSON file. You’ll have to design the JSON format yourself.

### Solution ###

* Task 1 displays the business card for the player when hovering over the players name.

![Task1 Solution](https://dl.dropboxusercontent.com/u/73204184/Task1.png)

## Task 2 ##

* During a game, if a player performs an important action such as score a goal, give an assist, take a corner, etc. record this on the page and post this data as a summary when the game is over. Add one extra feature based on what you think is important

### Solution ###

* The extra features which I have implemented are a timestamp of when the action happened (displayed in the summary) and a start game button which is displayed once the end game button is clicked.

![Task2 Solution](https://dl.dropboxusercontent.com/u/73204184/task2.png)
window.onload = function () {
    /**
     * Reads JSON from a file
     * @param {type} callback - for when loading JSON has completed
     */
    function loadJSON(callback) {
        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
        xobj.open('GET', 'player_data.json', true);
        xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == "200") {
                // return the file data in callback when loading is complete
                callback(xobj.responseText);
            }
        };
        xobj.send(null);
    }


    loadJSON(function (response) {
        var playersList = document.getElementById('players-list');
        // Parse JSON string into object
        var playersJson = JSON.parse(response).players;
        for (var i in playersJson) {
            var player = new Player(playersJson[i]);
            // append player row to the players list
            playersList.appendChild(player.getRow());
        }
    });
};



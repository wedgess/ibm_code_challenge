// template for each individual player
var viewTemplate = '<div class="player">\
                        <div class="player__name">{{name}}</div>\
                        <div class="player-details popup">\
                            <div class="player-details__name">{{name}}</div>\
                            <hr>\
                            <div class="player-skill__title">Skill</div>\
                            <div class="player-skill__rating">Rating</div>\
                            <div class="player-details__skills">{{skills}}</div>\
                        </div>\
                    </div>';

// skill templaye for each of the skills
var skillTemplate = '<div class="player-skill">\
                        <div class="player-skill__name">{{skillName}}</div>\
                        <div class="player-skill__rank">{{skillRank}}</div>\
                    </div>';

function Player(player) {
    this.name = player.name;
    this.skills = player.skills;
}

Player.prototype.getRow = function () {
    var skillTemplates = '';
    // create template element (HTML5)
    var playerListItem = document.createElement('template');
    var playerListTemplate = viewTemplate.replace(/{{name}}/g, this.name);

    // loop through skills, appending each filled in skill template
    for (var index in this.skills) {
        skillTemplates += getSkillTemplate(this.skills[index]);
    }
    playerListItem.innerHTML = playerListTemplate.replace(/{{skills}}/g, skillTemplates);

    // return first child of template in order to not append the template tag
    return playerListItem.content.firstChild;
}

function getSkillTemplate(skill) {
    // replace skill name and rank of the template with the passed in skill name and skill rank
    return skillTemplate.replace(/{{skillName}}/g, skill.skillName).replace(/{{skillRank}}/g, skill.skillRank);
}

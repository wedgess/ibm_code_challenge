window.onload = function () {
    // array for holding the games records
    var gameRecords = [];

    /**
     *  Reads JSON from file
     * @param {type} callback - for when loading JSON has completed
     */
    function loadJSON(callback) {
        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
        xobj.open('GET', 'player_data.json', true);
        xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == "200") {
                callback(xobj.responseText);
            }
        };
        xobj.send(null);
    }

    function initGameControls() {
        // game summary div
        var gameSummary = document.getElementById('game-summary');
        //  for displaying the game log
        var gameFeed = document.getElementById('game-feed');
        var startGameBtn = document.getElementById('start-game-btn');
        var endGameBtn = document.getElementById('end-game-btn');

        endGameBtn.addEventListener('click', function () {
            var feed = '';
            // make sure the records array is not empty (if empty show no actions text)
            if (gameRecords.length > 0) {
                for (var index in gameRecords) {
                    //console.log(gameRecords[i]);
                    // append each record to feed
                    feed += gameRecords[index];
                }
            } else {
                feed = "Nothing happened during this game!!"
            }

            gameFeed.innerHTML = feed;
            // show the summary div
            gameSummary.style.display = 'block';
            endGameBtn.style.display = 'none';
            startGameBtn.style.display = 'block';
        });


        startGameBtn.addEventListener('click', function () {
            // clear previous game records
            gameRecords = [];
            // remove game feed text
            gameFeed.innerHTML = '';
            // hide summary div
            gameSummary.style.display = 'none';
            endGameBtn.style.display = 'block';
            startGameBtn.style.display = 'none';
        });

    }

    function init() {
        initGameControls();
        loadJSON(function (response) {
            var playersList = document.getElementById('players-list');
            // Parse JSON string into object
            var playersJson = JSON.parse(response).players;
            for (var i in playersJson) {
                var player = new Player(playersJson[i]);
                //console.log(player);
                // append player row to the players list, has a callback 
                // for when one of the action buttons is pressed, its data is passed in here
                playersList.appendChild(player.getRow(function (actionString) {
                    //console.log(actionString);
                    // action button pressed, add the action string to the gameRecords array
                    gameRecords.push(actionString);
                }));
            }
        });
    }

    // kick start app
    init();
};



// template for each player row, buttons will be added to player__actions <div>
var viewTemplate = '<div class="player">\
                        <div class="player__name">{{name}}</td>\
                        <div class="player__actions">\
                        </div>\
                    </div>';

// log template for when user scores a goal
var actionTemplate = '<div class="{{actionClass}}">\
                        {{timestamp}}: {{name}} {{actionText}}\
                    </div>';

// map contains each of the action buttons, the text for the action and the
// class to be applied to the div template. 
// Note:: new actions should be added here
var actionMap = {
    'Score': {
        actionText: 'scored a goal!',
        actionClass: 'player-goal'
    },
    'Assist': {
        actionText: 'assisted a goal!',
        actionClass: 'player-assist'
    },
    'Corner': {
        actionText: 'is taking a corner!',
        actionClass: 'player-corner'
    },
    'Throw In': {
        actionText: 'is taking a throw in!',
        actionClass: 'player-throw-in'
    },
    'Penalty': {
        actionText: 'is taking a penalty!',
        actionClass: 'player-penalty'
    }
};

function Player(player) {
    this.name = player.name;
}

Player.prototype.getRow = function (actionCallback) {
    // create template element (HTML5)
    var playerListItem = document.createElement('template');
    // replace name placeholder inside the template with this players name
    var playerListTemplate = viewTemplate.replace(/{{name}}/g, this.name);

    // set the innerHTML on the player list
    playerListItem.innerHTML = playerListTemplate;

    var node = playerListItem.content.firstChild;
    // append each of the buttons for each object in the map
    for (var action in actionMap) {
        node.appendChild(createButton(action, this.name, actionCallback))
    }

    // return the row
    return node;
}

/**
 * Creates a new action button
 * 
 * @param {obj} action - action from actionMap
 * @param {string} name - players name
 * @param {type} callback - callback for action button (sent to task2.js)
 * @returns {createButton.btn|Element} - Button
 */
function createButton(action, name, callback) {
    // get the action based on its key from the actionMap
    var actionObj = actionMap[action];

    var btn = document.createElement("button");
    var btnTxt = document.createTextNode(action);
    btn.appendChild(btnTxt);
    // add event listener for this button
    btn.addEventListener('click', function () {
        // replace players name, timestamp and action text placeholders within template
        var actionString = actionTemplate.replace(/{{name}}/g, name).replace(/{{timestamp}}/g, new Date().toGMTString())
                .replace(/{{actionClass}}/g, actionObj.actionClass).replace(/{{actionText}}/g, actionObj.actionText);
        // send result back to taks2.js when button is clicked
        callback(actionString);
    });
    
    return btn;
}